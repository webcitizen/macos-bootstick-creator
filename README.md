# MacOS Bootstick Creator

### How to use
You need to download the macOS app into the Applications folder,
which is a default behavior when you fetch the latest macOS version via the Apple App Store.
And obviously a USB stick that you can spare with at least 8 GByte or so.
Just call the script and it is going to guide you trough the process.

    $ create-mac-stick

### My motivation
Just a simple convenience tool for my professional work as well as just another practical scripting training. :)
