#!/usr/bin/env bash
#
# MacOS Bootstick Creator v0.9
# tested against macOS 10.14.3, Bash 3.2.57(1)-release
#
# Christopher Engelmann, 2019
# https://github.com/webcitizen-de/MacOS-Bootstick-Creator


# ==== C O N F I G U R A T I O N =======================
  addon_directory="Extras"
  debug="false"
# ======================================================

clear

function listVolumes {
    echo "/// These are your current volumes"
    ls -1 /Volumes
    echo
}

function printHeader {
    clear
    echo
    echo "****************************************"
    echo "****  macOS Bootstick Creator v0.9 **** "
    echo "****************************************"
    echo
}


#1 VERIFY: the "macOS Installer Path" app

while [[ -z $installer_path ]]; do

    printHeader
    glob_filter="/Applications/Install*macOS*"
    amount_of_hits=$(ls -1d $glob_filter | wc -l | tr -d [:space:])

    # proceed only if you've got a single hit, otherwise ask the user
    if [[ $amount_of_hits -eq 1 ]]; then
        installer_path=$(ls -1d $glob_filter)

        echo "$installer_path/Contents/version.plist"
        echo
        macOS_installer_ver=$(
            /usr/bin/grep --after-context 1 CFBundleShortVersionString "$installer_path/Contents/version.plist" |
            /usr/bin/grep --extended-regexp --only-matching '\d*\.\d+\.*\d*'
        )

        echo "I presume this is your macOS installer app and going to use it:"
        echo "$installer_path – Version: $macOS_installer_ver"
        echo
        IFS=''
        while [[ "$reply" == "" ]]; do
            echo "Press Space to proceed"
            read -n 1 reply
        done
    else
        if [[ $? -ne 0 ]]; then
            echo "Please enter the full path of the macOS Installer (SOURCE)"
            read -p "--> " installer_path
        fi
    fi
done


#2 VERIFY: the volume name of the USB target

while [[ $bootstick_plugged != "y" ]]; do
    printHeader
    echo "Is the target USB stick plugged in?!"
    echo "Please make sure that it is and enter: y"
    read -p "--> " bootstick_plugged
done
echo

while [[ -z $volume_name ]]; do
    printHeader
    listVolumes
    echo "Please enter the volume name (TARGET)"
    read -p "--> " volume_name
done
volume_name=$(basename $volume_name)


#3 BACKUP: the USB Stick "$addon_directory" directory

if [[ -d /Volumes/$volume_name/$addon_directory ]]; then
    folder_available="true"
    echo "securing \"$addon_directory\" folder"
    echo

    rsync --recursive "/Volumes/$volume_name/$addon_directory" /tmp
    if [[ $? -ne 0 ]]; then
        echo "Error: something went wrong with the backup"
        exit
    fi

else
    if [[ -d /tmp/$addon_directory ]]; then
        echo "I couldn't find the \"$addon_directory\" folder on your USB stick"
        echo "Though I found \"$addon_directory\" in /tmp and using it instead"
        echo
        folder_available="true"
    else
        echo "I couldn't find any \"$addon_directory\" folder!"
    fi
fi


#4 CREATE: macOS Installer Stick

echo "Please enter your mac user password in order to format and initialize the boot stick"
sudo --reset-timestamp "$installer_path"/Contents/Resources/createinstallmedia --volume /Volumes/"$volume_name"


#5 VERIFY: the new volume name of the USB stick

if [[ $folder_available == "true" ]]; then

    while [[ -z $volume_name ]]; do
        printHeader

        # in case you haven't detect the new volume name ask the user
        volume_name=$(ls -1d /Volumes/Install\ macOS*)
        if [[ $? -ne 0 ]]; then
            listVolumes
            echo "Please enter the new volume name of the USB stick (TARGET)"
            read -p "--> " volume_name
        fi
    done


    #6 RESTORE: the USB Stick "$addon_directory" directory

    if [[ -d /tmp/$addon_directory ]]; then

        echo "restoring \"$addon_directory\" folder..."
        if [[ debug == "true" ]]; then
            echo "rsync target is $volume_name"
            rsync $rsync_options_restore --verbose "/tmp/$addon_directory" "/Volumes/$volume_name"
            echo "rsync exit status is: $?"
        else
            rsync --recursive "/tmp/$addon_directory" "/Volumes/$volume_name"
        fi
        echo

        if [[ $? -eq 0 ]]; then
            echo "Done. Your macOS installer stick is good to go"
        else
            echo "Error: something went wrong while restoring"
        fi
    fi
fi
